#remmeber indentation is the way to nest the code in Python
import random

def main():
    print ('Welcome To Guessing Game')
    
    name = input('Please Enter Your Name')
    if name == 'Paul':
        exit()
    userguess = ''
    compguess = str(random.randint(0,9))
    
    print (compguess)
    
    print ('This Game Is About Guessing The Number (0,9)')
    
    #To break program excution press Ctrl + C
    while userguess != compguess :
        userguess = input ('Enter Your Guess')
        
    print (random.choice(['You Won!','Well Done!','You Made It!']))
    
if __name__ == '__main__':
    main()